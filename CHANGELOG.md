# Change Log

## [v4.2.1](https://github.com/strator-dev/greenpepper/tree/v4.2.1) (2017-12-10)

[Full Changelog](https://github.com/strator-dev/greenpepper/compare/v4.2.0...v4.2.1)

**Implemented enhancements:**

- Maven plugin reports type is not relevant anymore [\#118](https://github.com/strator-dev/greenpepper/issues/118)

**Fixed bugs:**

- GreenPepper client conflicts due to org/xml/sax [\#121](https://github.com/strator-dev/greenpepper/issues/121)

**Closed issues:**

- Update mvn plugin documentation [\#53](https://github.com/strator-dev/greenpepper/issues/53)

**Merged pull requests:**

- Drop greenpepper license project [\#120](https://github.com/strator-dev/greenpepper/pull/120) ([wattazoum](https://github.com/wattazoum))

## [v4.2.0](https://github.com/strator-dev/greenpepper/tree/v4.2.0) (2017-11-28)

[Full Changelog](https://github.com/strator-dev/greenpepper/compare/v4.1.8...v4.2.0)

**Implemented enhancements:**

- Support The Markdown syntax in GreenPepper Specs [\#63](https://github.com/strator-dev/greenpepper/issues/63)

**Fixed bugs:**

- Hard coded dependencies on java classpath command line [\#115](https://github.com/strator-dev/greenpepper/issues/115)

**Merged pull requests:**

- Features/63 markdown syntax [\#117](https://github.com/strator-dev/greenpepper/pull/117) ([wattazoum](https://github.com/wattazoum))

## [v4.1.8](https://github.com/strator-dev/greenpepper/tree/v4.1.8) (2017-10-12)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/v4.1.7...v4.1.8)

**Implemented enhancements:**

- Make the log link a `a` instead of a `button` so a middle click can open a new tab displaying the log file [\#111](https://github.com/strator-dev/greenpepper/issues/111)

**Fixed bugs:**

- confluence table headers can't be selected [\#113](https://github.com/strator-dev/greenpepper/issues/113)
- When opening and closing spec lines from  the report, the line colors change in a rather disturbing manner [\#112](https://github.com/strator-dev/greenpepper/issues/112)
- Make the log link a `a` instead of a `button` so a middle click can open a new tab displaying the log file [\#111](https://github.com/strator-dev/greenpepper/issues/111)

**Merged pull requests:**

- Hotfixes/115 conflicts on dependencies  [\#116](https://github.com/strator-dev/greenpepper/pull/116) ([wattazoum](https://github.com/wattazoum))
- project-select-button [\#110](https://github.com/strator-dev/greenpepper/pull/110) ([arnor2000](https://github.com/arnor2000))

## [v4.1.7](https://github.com/strator-dev/greenpepper/tree/v4.1.7) (2017-07-17)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/v4.1.6...v4.1.7)

**Implemented enhancements:**

- Extract the angular html report from the java source code in order to be able to factorise the report with the .Net implementation [\#101](https://github.com/strator-dev/greenpepper/issues/101)
- Add a "Run" filter in the filter combo in the html report [\#100](https://github.com/strator-dev/greenpepper/issues/100)
- Add html report state as hash tag on the url [\#99](https://github.com/strator-dev/greenpepper/issues/99)
- List Validation - Improvements [\#97](https://github.com/strator-dev/greenpepper/issues/97)
- Pleasing ESLint with === operator [\#106](https://github.com/strator-dev/greenpepper/pull/106) ([gissehel](https://github.com/gissehel))
- Add a new "Run" filter to filter status 'succeeded', 'failed' and 'skipped' [\#103](https://github.com/strator-dev/greenpepper/pull/103) ([gissehel](https://github.com/gissehel))
- Add meta data on html table's td to help easily identify success, faillure and other status in page [\#96](https://github.com/strator-dev/greenpepper/pull/96) ([gissehel](https://github.com/gissehel))

**Fixed bugs:**

- Global Exception makes the build hangs [\#95](https://github.com/strator-dev/greenpepper/issues/95)

**Merged pull requests:**

- Features/99 stateinhtmlreporturl [\#109](https://github.com/strator-dev/greenpepper/pull/109) ([wattazoum](https://github.com/wattazoum))
- Features/101 extract htmlreport [\#108](https://github.com/strator-dev/greenpepper/pull/108) ([wattazoum](https://github.com/wattazoum))
- Features/97 collections column validation [\#107](https://github.com/strator-dev/greenpepper/pull/107) ([wattazoum](https://github.com/wattazoum))
- Gissehel feature/meta data status [\#105](https://github.com/strator-dev/greenpepper/pull/105) ([wattazoum](https://github.com/wattazoum))
- Avoid global exception if the spec is malformed [\#104](https://github.com/strator-dev/greenpepper/pull/104) ([wattazoum](https://github.com/wattazoum))
- Add a shortcut icon the report [\#98](https://github.com/strator-dev/greenpepper/pull/98) ([gissehel](https://github.com/gissehel))

## [v4.1.6](https://github.com/strator-dev/greenpepper/tree/v4.1.6) (2017-06-23)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/v4.1.5...v4.1.6)

**Implemented enhancements:**

- backport issue 90 - separate SUD args [\#92](https://github.com/strator-dev/greenpepper/issues/92)
- Allow the configuration of the SUD class and params separately [\#90](https://github.com/strator-dev/greenpepper/issues/90)

**Merged pull requests:**

- Hotfixes/92 sud args in maven [\#93](https://github.com/strator-dev/greenpepper/pull/93) ([wattazoum](https://github.com/wattazoum))
- Support SUD ARGS in a separate parameter in Maven plugins [\#91](https://github.com/strator-dev/greenpepper/pull/91) ([wattazoum](https://github.com/wattazoum))

## [v4.1.5](https://github.com/strator-dev/greenpepper/tree/v4.1.5) (2017-02-19)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/v4.1.4...v4.1.5)

**Implemented enhancements:**

- Specify in generate-fixtures the default package [\#81](https://github.com/strator-dev/greenpepper/issues/81)
- Allow setting JAVA\_OPTS on maven plugin [\#77](https://github.com/strator-dev/greenpepper/issues/77)

**Fixed bugs:**

- runnerName on repository don't prevent default runner to be used [\#78](https://github.com/strator-dev/greenpepper/issues/78)
- Différence de comportement entre List of et Set of pour une grille vide [\#76](https://github.com/strator-dev/greenpepper/issues/76)
- Some runner thread might die when there is a global exception [\#60](https://github.com/strator-dev/greenpepper/issues/60)

**Closed issues:**

- Clean up the builds warnings [\#83](https://github.com/strator-dev/greenpepper/issues/83)

**Merged pull requests:**

- 83 clean build [\#85](https://github.com/strator-dev/greenpepper/pull/85) ([wattazoum](https://github.com/wattazoum))
- 81 defaultpackage in generatefixutre [\#84](https://github.com/strator-dev/greenpepper/pull/84) ([wattazoum](https://github.com/wattazoum))
- Add javaoptions to runners [\#80](https://github.com/strator-dev/greenpepper/pull/80) ([wattazoum](https://github.com/wattazoum))
- Conditional build [\#79](https://github.com/strator-dev/greenpepper/pull/79) ([wattazoum](https://github.com/wattazoum))

## [v4.1.4](https://github.com/strator-dev/greenpepper/tree/v4.1.4) (2016-12-20)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/v4.1.3...v4.1.4)

**Implemented enhancements:**

- Force the report page in user specified encoding [\#72](https://github.com/strator-dev/greenpepper/issues/72)
- allow 3 ways filtering on implemented state  in the greenpepper report [\#71](https://github.com/strator-dev/greenpepper/issues/71)

**Fixed bugs:**

- Missing log file on a greenpepper:run  [\#70](https://github.com/strator-dev/greenpepper/issues/70)

**Merged pull requests:**

- add charset to the report page [\#75](https://github.com/strator-dev/greenpepper/pull/75) ([wattazoum](https://github.com/wattazoum))
- Use select box for filtering implemented state [\#74](https://github.com/strator-dev/greenpepper/pull/74) ([wattazoum](https://github.com/wattazoum))
- 70 force logfile [\#73](https://github.com/strator-dev/greenpepper/pull/73) ([wattazoum](https://github.com/wattazoum))

## [v4.1.3](https://github.com/strator-dev/greenpepper/tree/v4.1.3) (2016-12-15)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/v4.1.2...v4.1.3)

**Fixed bugs:**

- Tag can't publish build to OSSRH [\#68](https://github.com/strator-dev/greenpepper/issues/68)

**Merged pull requests:**

- Add Changelog [\#69](https://github.com/strator-dev/greenpepper/pull/69) ([wattazoum](https://github.com/wattazoum))

## [v4.1.2](https://github.com/strator-dev/greenpepper/tree/v4.1.2) (2016-12-15)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/v4.1.1...v4.1.2)

**Implemented enhancements:**

- Improve the summary on a greenpepper:run build [\#62](https://github.com/strator-dev/greenpepper/issues/62)

**Fixed bugs:**

- Slf4J Logs when using greenpepper-extensions-java [\#57](https://github.com/strator-dev/greenpepper/issues/57)

**Merged pull requests:**

- Fix a replacement issue when placeholders are quoted [\#67](https://github.com/strator-dev/greenpepper/pull/67) ([arnor2000](https://github.com/arnor2000))
- Issue 62 mvnplugin summary [\#66](https://github.com/strator-dev/greenpepper/pull/66) ([arnor2000](https://github.com/arnor2000))
- Issue 57 slf4j logs [\#64](https://github.com/strator-dev/greenpepper/pull/64) ([wattazoum](https://github.com/wattazoum))

## [v4.1.1](https://github.com/strator-dev/greenpepper/tree/v4.1.1) (2016-08-16)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/v4.1...v4.1.1)

**Fixed bugs:**

- In the report, sometimes the results are not loaded correctly [\#59](https://github.com/strator-dev/greenpepper/issues/59)
- Running a specification with the main thread seems not to generate logs file. [\#58](https://github.com/strator-dev/greenpepper/issues/58)

## [v4.1](https://github.com/strator-dev/greenpepper/tree/v4.1) (2016-08-15)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/v4.0.4...v4.1)

**Implemented enhancements:**

- Maven plugin should be able to run non implemented versions [\#55](https://github.com/strator-dev/greenpepper/issues/55)
- Add the implemented state of a Specification in maven report [\#54](https://github.com/strator-dev/greenpepper/issues/54)
- Create a test summary index page [\#49](https://github.com/strator-dev/greenpepper/issues/49)
- Attlassian Repository should throw a DocumentNotFound [\#46](https://github.com/strator-dev/greenpepper/issues/46)
- Generate Fixture documentation  [\#44](https://github.com/strator-dev/greenpepper/issues/44)
- Log more information about the execution of a specification [\#43](https://github.com/strator-dev/greenpepper/issues/43)
- Option DevMode [\#8](https://github.com/strator-dev/greenpepper/issues/8)
- Build incremental [\#7](https://github.com/strator-dev/greenpepper/issues/7)
- Filter from a repository [\#6](https://github.com/strator-dev/greenpepper/issues/6)
- Fork on Maven test [\#5](https://github.com/strator-dev/greenpepper/issues/5)

**Fixed bugs:**

- When using the forked mode, with a special char, there is an exception [\#47](https://github.com/strator-dev/greenpepper/issues/47)
- generate-fixtures sometimes fails after generating the fixtures [\#41](https://github.com/strator-dev/greenpepper/issues/41)

**Closed issues:**

- Publish somewhere the maven plugin documentation [\#39](https://github.com/strator-dev/greenpepper/issues/39)
- Exception during test but nothing in the logs [\#38](https://github.com/strator-dev/greenpepper/issues/38)
- Generate Fixtures out of a Specification [\#11](https://github.com/strator-dev/greenpepper/issues/11)
- See SUT specifications tree [\#10](https://github.com/strator-dev/greenpepper/issues/10)
- Migrate the GreenPepper Eclipse plugin to Maven [\#9](https://github.com/strator-dev/greenpepper/issues/9)

**Merged pull requests:**

- Flexibility to gptree [\#56](https://github.com/strator-dev/greenpepper/pull/56) ([wattazoum](https://github.com/wattazoum))
- Upload doc to confluence [\#52](https://github.com/strator-dev/greenpepper/pull/52) ([wattazoum](https://github.com/wattazoum))
- Html report page [\#51](https://github.com/strator-dev/greenpepper/pull/51) ([wattazoum](https://github.com/wattazoum))
- Incremental test [\#50](https://github.com/strator-dev/greenpepper/pull/50) ([wattazoum](https://github.com/wattazoum))
- Maven plugin jvmfork [\#48](https://github.com/strator-dev/greenpepper/pull/48) ([wattazoum](https://github.com/wattazoum))
- Documentation annotation [\#45](https://github.com/strator-dev/greenpepper/pull/45) ([wattazoum](https://github.com/wattazoum))
- Generate fixtures [\#40](https://github.com/strator-dev/greenpepper/pull/40) ([wattazoum](https://github.com/wattazoum))
- Specification tree management [\#37](https://github.com/strator-dev/greenpepper/pull/37) ([wattazoum](https://github.com/wattazoum))

## [v4.0.4](https://github.com/strator-dev/greenpepper/tree/v4.0.4) (2016-05-28)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/v4.0.3...v4.0.4)

**Merged pull requests:**

- Fuse gp and gpopen repos [\#36](https://github.com/strator-dev/greenpepper/pull/36) ([wattazoum](https://github.com/wattazoum))

## [v4.0.3](https://github.com/strator-dev/greenpepper/tree/v4.0.3) (2016-05-28)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/v4.0.1...v4.0.3)

## [v4.0.1](https://github.com/strator-dev/greenpepper/tree/v4.0.1) (2016-05-28)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/v4.0.2...v4.0.1)

**Fixed bugs:**

- Change the order of the TypeConverter lookup method [\#29](https://github.com/strator-dev/greenpepper/issues/29)

## [v4.0.2](https://github.com/strator-dev/greenpepper/tree/v4.0.2) (2016-05-16)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/v4.0...v4.0.2)

**Merged pull requests:**

- Deploy releases from travisci [\#28](https://github.com/strator-dev/greenpepper/pull/28) ([wattazoum](https://github.com/wattazoum))

## [v4.0](https://github.com/strator-dev/greenpepper/tree/v4.0) (2016-03-26)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/v4.0-RC1...v4.0)

**Fixed bugs:**

- HTML head base tag breaks exceptions folding [\#27](https://github.com/strator-dev/greenpepper/issues/27)

## [v4.0-RC1](https://github.com/strator-dev/greenpepper/tree/v4.0-RC1) (2016-02-05)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/v4.0-beta2...v4.0-RC1)

**Implemented enhancements:**

- Be more flexible when Ending a DoWith [\#22](https://github.com/strator-dev/greenpepper/issues/22)
- Display all exception stacktrace in page result [\#21](https://github.com/strator-dev/greenpepper/issues/21)

**Fixed bugs:**

- maven-plugin fails to retreive the Specifications when not in the project basedir [\#26](https://github.com/strator-dev/greenpepper/issues/26)
- cli-runner can override the input spec with its output [\#25](https://github.com/strator-dev/greenpepper/issues/25)
- NoSuchMethodError when launching from GreenPepper runner [\#24](https://github.com/strator-dev/greenpepper/issues/24)
- Error message a little bit misleading on SUD init error [\#16](https://github.com/strator-dev/greenpepper/issues/16)

**Closed issues:**

- Log exceptions when they occurs in fixture call [\#20](https://github.com/strator-dev/greenpepper/issues/20)
- Throw DocumentNotFoundException when page is not found in AtlassianRepository [\#19](https://github.com/strator-dev/greenpepper/issues/19)

**Merged pull requests:**

- Handle end correctly in do with [\#23](https://github.com/strator-dev/greenpepper/pull/23) ([wattazoum](https://github.com/wattazoum))

## [v4.0-beta2](https://github.com/strator-dev/greenpepper/tree/v4.0-beta2) (2015-12-01)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/v2.11-beta1...v4.0-beta2)

## [v2.11-beta1](https://github.com/strator-dev/greenpepper/tree/v2.11-beta1) (2015-11-30)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/greenpepper-base-4.0-beta2...v2.11-beta1)

## [greenpepper-base-4.0-beta2](https://github.com/strator-dev/greenpepper/tree/greenpepper-base-4.0-beta2) (2015-11-25)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/2.10.1...greenpepper-base-4.0-beta2)

**Implemented enhancements:**

- Simplify the XML RPC arguments requirements [\#18](https://github.com/strator-dev/greenpepper/issues/18)
- Exceptions in the results pages take too much place [\#15](https://github.com/strator-dev/greenpepper/issues/15)

**Fixed bugs:**

- Reg: greenpepper rpc doesn't flag pages as executable [\#17](https://github.com/strator-dev/greenpepper/issues/17)

**Closed issues:**

- Execute a single page [\#12](https://github.com/strator-dev/greenpepper/issues/12)

## [2.10.1](https://github.com/strator-dev/greenpepper/tree/2.10.1) (2015-09-25)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/4.0-beta1...2.10.1)

**Closed issues:**

- create a public repository for greenpepper [\#14](https://github.com/strator-dev/greenpepper/issues/14)
- Move Confluence Plugins to greenpepper Open [\#13](https://github.com/strator-dev/greenpepper/issues/13)

## [4.0-beta1](https://github.com/strator-dev/greenpepper/tree/4.0-beta1) (2015-09-22)
[Full Changelog](https://github.com/strator-dev/greenpepper/compare/greenpepper-base-2.10...4.0-beta1)

## [greenpepper-base-2.10](https://github.com/strator-dev/greenpepper/tree/greenpepper-base-2.10) (2015-09-19)
**Closed issues:**

- \[greenpepper-license\] dependency not found [\#2](https://github.com/strator-dev/greenpepper/issues/2)

**Merged pull requests:**

- Build the project [\#3](https://github.com/strator-dev/greenpepper/pull/3) ([sjeandeaux](https://github.com/sjeandeaux))
- Add a Gitter chat badge to README.md [\#1](https://github.com/strator-dev/greenpepper/pull/1) ([gitter-badger](https://github.com/gitter-badger))



\* *This Change Log was automatically generated by [github_changelog_generator](https://github.com/skywinder/Github-Changelog-Generator)*