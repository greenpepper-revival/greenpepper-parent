#!/bin/bash

set -x

case $TRAVIS_BRANCH in
  master|release-4.0.x|release-4.1.x)
    mvn clean deploy -B -Prelease | sed -r -e '/\[INFO\] Download(ing|ed).*/d'
    exit ${PIPESTATUS[0]}
  ;;
  *)
    if [ -n "$TRAVIS_TAG" ]
    then
      mvn clean deploy -B -Prelease | sed -r -e '/\[INFO\] Download(ing|ed).*/d'
      exit ${PIPESTATUS[0]}
    else
      mvn clean install -B | sed -r -e '/\[INFO\] Download(ing|ed).*/d'
      exit ${PIPESTATUS[0]}
    fi
  ;;
esac


