#!/bin/bash

[ -z "$1" ] && echo "usage: $0 <version> " && exit 1

VERSION="$1"

REPO=http://central.maven.org/maven2/com/github/strator-dev/greenpepper
OUTPUT=${OUTPUT:-demopack}

rm -rf $OUTPUT
mkdir -p $OUTPUT

curl -XGET $REPO/greenpepper-client/$VERSION/greenpepper-client-$VERSION-complete.jar \
    -o $OUTPUT/greenpepper-client-$VERSION-complete.jar
curl -XGET $REPO/greenpepper-samples-application/$VERSION/greenpepper-samples-application-$VERSION.jar \
    -o $OUTPUT/greenpepper-samples-application-$VERSION.jar
curl -XGET $REPO/greenpepper-samples-application/$VERSION/greenpepper-samples-application-$VERSION-sources.jar \
    -o $OUTPUT/greenpepper-samples-application-$VERSION-sources.jar
curl -XGET $REPO/greenpepper-confluence-demo/$VERSION/greenpepper-confluence-demo-$VERSION-fixtures.jar \
    -o $OUTPUT/greenpepper-confluence-demo-$VERSION-fixtures.jar


curl -XGET $REPO/greenpepper-confluence5-plugin/$VERSION/greenpepper-confluence5-plugin-$VERSION.obr \
    -o $OUTPUT/greenpepper-confluence5-plugin-$VERSION.obr
curl -XGET $REPO/greenpepper-extensions-java/$VERSION/greenpepper-extensions-java-$VERSION.jar \
    -o $OUTPUT/greenpepper-extensions-java-$VERSION.jar
curl -XGET $REPO/greenpepper-core/$VERSION/greenpepper-core-$VERSION.jar \
    -o $OUTPUT/greenpepper-core-$VERSION.jar
curl -XGET $REPO/greenpepper-remote-agent/$VERSION/greenpepper-remote-agent-$VERSION.jar \
    -o $OUTPUT/greenpepper-remote-agent-$VERSION.jar
curl -XGET $REPO/greenpepper-maven-runner/$VERSION/greenpepper-maven-runner-$VERSION-complete.jar \
    -o $OUTPUT/greenpepper-maven-runner-$VERSION-complete.jar

sed -e "s/@@project.version@@/$VERSION/" run.sh.tmpl > $OUTPUT/run.sh
chmod +x $OUTPUT/run.sh

cp -a project $OUTPUT/
sed -i -e "s/@@project.version@@/$VERSION/" $OUTPUT/project/pom.xml

cp -a README.md $OUTPUT/
sed -i -e "s/@@project.version@@/$VERSION/" $OUTPUT/README.md