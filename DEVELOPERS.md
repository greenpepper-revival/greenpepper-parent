# Build with Maven and Docker

```sh
docker run -v "$(pwd)":/usr/src/mymaven -w /usr/src/mymaven -v /windows/dev/mvnrepo:/home/gpbuilder/.m2 --rm zoomonit/greenpepper-builder mvn clean install 
```